import axios, { AxiosPromise, AxiosRequestConfig } from 'axios'
import { getCookie } from '#/storage/cookie/index'
import { $t } from '@/plugins/i18n'
import { toNext } from '@/Router'

const baseURL = '//' + window.config.url
const prefix = '/api/v3'
const timeout = 50000
const requestBase = axios.create({
  baseURL,
  timeout,
  headers: {
    'Cache-Control': 'no-cache',
  },
})

//响应拦截器
requestBase.interceptors.response.use(
  //success
  (response) => {
    if (response.data && response.data.status !== 'SUCCESS') {
      return Promise.reject(response)
    }
    return Promise.resolve(response)
  },
  (error) => {
    return Promise.reject(error)
  },
)

/**
 * 是否需要检测token，token存储在Cookie上
 * @param {string} url 接口路径
 * @returns {boolean}
 */
function isCheckCookie(url: string | undefined): boolean {
  if (!url) return false
  const u = url.split('/')[0]
  if (['login'].includes(u)) return false

  return true
}

/**
 * 请求
 * @param {AxiosRequestConfig} Config 配置
 * @returns {function} 请求
 */
function request<T>(Config: AxiosRequestConfig): AxiosPromise | Promise<T> {
  const Authorization = getCookie('token')
  if (isCheckCookie(Config.url) && !Authorization) {
    toNext({
      name: 'Login',
    })
    return new Promise<T>((resolve, reject) => {
      reject($t('message.tokenTimeOut'))
    })
  }
  if (Config.baseURL) {
    requestBase.defaults.baseURL = Config.baseURL
  } else {
    requestBase.defaults.baseURL = baseURL
  }
  Config.headers = { Authorization }
  Config.url = `${prefix}${Config.url}`

  return requestBase(Config)
}

export default request
