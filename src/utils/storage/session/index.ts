function sessionDB(name: string): string | null
function sessionDB(name: string, value: string): void
/**
 * sessionStorage
 * @param name
 * @param value
 */
function sessionDB(name: string, value?: string) {
  if (value !== undefined) {
    sessionStorage.setItem(name, value)
  } else {
    return sessionStorage.getItem(name)
  }
}

sessionDB('')
