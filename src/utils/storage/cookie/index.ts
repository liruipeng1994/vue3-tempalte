/**
 * 写cookie
 * @param {String} name 键
 * @param {String} value 值
 * @param {Number} time cookie过期时间戳
 */
export function setCookie(name: string, value: string, time: number): void {
  let str = name + '=' + escape(value)
  if (time > 0) {
    const date = new Date()
    date.setTime(date.getTime() + time)
    str += '; expires=' + date.toUTCString()
  }
  if (time === Infinity) {
    str += '; expires=Fri, 31 Dec 9999 23:59:59 GMT'
  }
  str += '; path=/'
  document.cookie = str
}

/**
 * 读cookie
 * @param {String} name 键
 * @returns {String} 值
 */
export function getCookie(name: string): string {
  const arrStr = document.cookie.split('; ')
  for (const item of arrStr) {
    const temp = item.split('=')
    if (temp[0] === name) return unescape(temp[1])
  }
  return ''
}

/**
 * 清除cookie
 * @param {String} name 键
 */
export function clearCookie(name: string): void {
  setCookie(name, '', 0)
}
