export const prefix = '/api/v3/'

export function exportFile(filePath: string): void {
  window.open(`//${window.config.url}${prefix}${filePath}`)
}

export type Tstate = {
  [n: string]: unknown
}
type Kstate = keyof Tstate
/**
 *  覆盖原对象属性方法，返回原对象类型
 * @param baseState 原对象
 * @param state 新对象
 * @returns
 */
export function mixinsExtend(baseState: Tstate, state: Tstate): Tstate {
  const result = baseState
  for (const key in state) {
    if (Object.prototype.hasOwnProperty.call(result, key) && Object.prototype.hasOwnProperty.call(state, key)) {
      const k = key as Kstate
      result[k] = state[k]
    }
  }
  return result
}

/**
 * 四舍五入保留n位小数
 * @param {string} str 修改的数字
 * @param {Number} n 保留的位数
 * @param {String} returnType 返回类型：0数字，1字符串
 * @param {String} round 四舍五入round，上取整floor，下取整ceil
 * @returns {String} 结果
 */
export function fomatFloat(str: string, n: number, returnType = 0, round = 'round'): void | string | number {
  const num = parseFloat(str)
  if (isNaN(num)) return 'NaN'
  let f
  switch (round) {
    // 上取整
    case 'floor': {
      f = Math.floor(num * Math.pow(10, n)) / Math.pow(10, n) // n 幂
      break
    }
    // 下取整
    case 'ceil': {
      f = Math.ceil(num * Math.pow(10, n)) / Math.pow(10, n) // n 幂
      break
    }
    // 四舍五入
    default: {
      f = Math.round(num * Math.pow(10, n)) / Math.pow(10, n) // n 幂
    }
  }
  if (!returnType) return f
  let s = f.toString()
  let rs = s.indexOf('.')
  // 判定如果是整数，增加小数点再补0
  if (rs < 0) {
    rs = s.length
    s += '.'
  }
  while (s.length <= rs + n) {
    s += '0'
  }
  return s
}

/**
 * 富文本json
 * @param {Object} json json
 * @returns {null | String} 转换结果
 */
export function richTextJson<T>(json: T): string | null {
  if (!json) {
    return null
  }
  return JSON.stringify(json, undefined, 2)
    .replace(/&/g, '&')
    .replace(/</g, '<')
    .replace(/>/g, '>')
    .replace(
      /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+-]?\d+))/g,
      function (match) {
        let color = 'darkorange'
        if (/^"/.test(match)) {
          color = /:$/.test(match) ? 'red' : 'green'
        } else if (/true|false/.test(match)) {
          color = 'blue'
        } else if (/null/.test(match)) {
          color = 'magenta'
        }
        return '<span style="color: ' + color + ';">' + match + '</span>'
      },
    )
}

/**
 * 获取随机位数的字符串
 * @param {Boolean} randomFlag true时，输入min，max获取区间内的随机位数；false时，再输入一个变量，获取固定位数的字符串
 */
export function randomWord(randomFlag: boolean, min: number, max: number): string {
  let str = '',
    range = min,
    pos = 0

  const arr = [
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
  ]

  // 随机产生
  if (randomFlag) {
    range = Math.round(Math.random() * (max - min)) + min
  }
  for (let i = 0; i < range; i++) {
    pos = Math.round(Math.random() * (arr.length - 1))
    str += arr[pos]
  }
  return str
}

// Object.prototype.toString
export function getType(obj: any): string {
  const type = typeof obj
  if (type !== 'object') {
    return type
  }
  return Object.prototype.toString.call(obj).replace(/^\[object (\S+)\]$/, '$1')
}

/**
 * 浅拷贝
 * @param target 拷贝对象
 * @returns any
 */
export function shallowClone(target: any): any {
  const type = getType(target)
  if (['Object', 'Array'].includes(type)) {
    const cloneTarget: any = Array.isArray(target) ? [] : {}
    for (const key in target) {
      if (Object.hasOwnProperty.call(target, key)) {
        cloneTarget[key] = target[key]
      }
    }
    return cloneTarget
  } else {
    return target
  }
}
/**
 * 是否为对象或函数
 * @param obj any
 * @returns boolean
 */
export function isComplexDataType(obj: any): boolean {
  return (typeof obj === 'object' || typeof obj === 'function') && obj !== null
}
/**
 * 深拷贝
 * @param {any} obj 拷贝对象
 * @param {WeekMap} hash hash对象
 */
export function deepClone(obj: any, hash = new WeakMap()): any {
  if (typeof obj === 'string') {
    return obj
  }
  if (typeof obj === 'number') {
    return obj
  }
  if (typeof obj === 'boolean') {
    return obj
  }
  if (typeof obj === 'undefined') {
    return obj
  }
  if (obj.constructor === Date) {
    return new Date(obj)
  }
  if (obj.constructor === RegExp) {
    return new RegExp(obj)
  }
  if (hash.has(obj)) return hash.get(obj)
  const allDesc = Object.getOwnPropertyDescriptor(obj, '') as PropertyDescriptorMap & ThisType<any>
  // 遍历传入参数所有键特性
  const cloneObj = Object.create(Object.getPrototypeOf(obj), allDesc)
  // 继承原型链
  hash.set(obj, cloneObj)
  for (const key of Reflect.ownKeys(obj)) {
    cloneObj[key] = isComplexDataType(obj[key]) && typeof obj[key] !== 'function' ? deepClone(obj[key], hash) : obj[key]
  }
  return cloneObj
}

/**
 * 是否为数组类型
 * @param obj any
 * @returns boolean
 */
export function isArray(obj: any): boolean {
  return obj instanceof Array
}

/**
 * 是否为对象且不为数组类型
 * @param obj any
 * @returns boolean
 */
export function isObject(obj: any): boolean {
  return typeof obj === 'object' && !isArray(obj)
}

/**
 * 是否为Number类型
 * @param obj any
 * @returns boolean
 */
export function isNumber(obj: any): boolean {
  return typeof obj === 'number'
}

/**
 * 是否为String类型
 * @param obj any
 * @returns boolean
 */
export function isString(obj: any): boolean {
  return typeof obj === 'string'
}

/**
 * 是否为Boolean类型
 * @param obj any
 * @returns boolean
 */
export function isBoolean(obj: any): boolean {
  return typeof obj === 'boolean'
}
