function localDB(name: string): string | null
function localDB(name: string, value: string): void
/**
 * 本地存储
 * @param name
 * @param value
 */
function localDB(name: string, value?: string) {
  if (value !== undefined) {
    localStorage.setItem(name, value)
  } else {
    return localStorage.getItem(name)
  }
}
/**
 * 用户信息，固定key: UserLocalData
 * @param {string} value value
 */
export const UserLocalData = function (value?: string): string | null | void {
  if (value !== undefined) {
    localDB('UserLocalData', value)
  } else {
    return localDB('UserLocalData')
  }
}

/**
 *  i18n语言
 * @param value value
 * @returns
 */
export const i18nLocalData = function (value?: string): string | null | void {
  if (value !== undefined) {
    localDB('i18nLocalData', value)
  } else {
    return localDB('i18nLocalData')
  }
}
