import { createApp } from 'vue'
import App from './App.vue'
import router from './Router'
import i18n from '@/assets/plugins/i18n'
import '@/assets/style/index.less'

const app = createApp(App)
app.use(i18n)
app.use(router)
router.isReady().then(() => app.mount('#app'))
