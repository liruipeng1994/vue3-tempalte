import {
  createRouter,
  createWebHashHistory,
  RouteRecordRaw,
  Router,
  RouteLocationRaw,
  RouteLocationNormalizedLoaded,
  LocationQueryValue,
} from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/Index',
    name: 'Index',
    component: () => import(/*webpackChunkName:"Index"*/ '@/pages/Index.vue'),
  },
  {
    path: '/',
    redirect: '/Index',
  },
]

const router: Router = createRouter({
  history: createWebHashHistory(),
  routes,
})

/**
 * 路由跳转
 * @param to 下一个路由参数
 */
export function toNext(to: RouteLocationRaw): void {
  router.push(to)
}

/**
 * 返回
 */
export function back(): void {
  router.back()
}

/**
 * 获取当前路由
 * @returns
 */
export function getRoute(): RouteLocationNormalizedLoaded {
  return router.currentRoute.value
}

/**
 * 获取路由query参数类型s参数
 * @param type query参数类型
 * @returns
 */

export function getQuery(type: string): LocationQueryValue | LocationQueryValue[] {
  const $route = getRoute()
  return $route.query[type]
}

/**
 * 获取路由params参数
 * @param type params参数类型
 * @returns
 */

export function getParams(type: string): string | string[] {
  const $route = getRoute()
  return $route.params[type]
}

export default router
