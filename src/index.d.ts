interface WindowConfig {
  url: string
}
interface Window {
  config: WindowConfig
}
