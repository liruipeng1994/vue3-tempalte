import ENGLISH from './en'
import CHINESE from './cn'

export const messages = {
  ENGLISH,
  CHINESE,
}
