import { createI18n } from 'vue-i18n'
import { messages } from './langs'
import { i18nLocalData } from '#/storage/local'
export * from './langs'
const localLocale = i18nLocalData()
const defaultLocale = 'ENGLISH'
export const localeKey = localLocale ? localLocale : defaultLocale
export const i18n = createI18n({
  locale: localeKey,
  fallbackLocale: defaultLocale,
  messages,
  legacy: true,
  globalInjection: true,
})

export const $t = i18n.global.t
/**
 * 切换语言
 * @param localeKey 语言标识
 * @param locale element-plus locale
 */
export function setI18nLocale(newLocaleKey = localeKey): void {
  i18n.global.locale = newLocaleKey
  // i18nLocalData(newLocaleKey)
  document.title = i18n.global.t('message.title')
}
/**
 * 获取语言标识
 * @returns 返回当前使用的语言（标识）
 */
export function getI18nLocale(): string {
  return i18n.global.locale
}

export default i18n
