module.exports = {
  tabWidth: 2,
  useTabs: false,
  printWidth: 120,
  semi: false,
  singleQuote: true,
  vueIndentScriptAndStyle: true,
  arrowParens: 'avoid',
  trailingComma: 'all', // 尾随逗号
  bracketSpacing: true, // 对象中打印空格
  jsxSingleQuote: false,
  // jsxBracketSameLine: false, // 在jsx中把'>' 放同一行
  insertPragma: false, // 插入@format标识
  arrowParens: 'always',
  endOfLine: 'auto',
  quoteProps: 'as-needed',
  ignorePath: '.prettierignore',
}
