# 项目背景

项目模板

## 技术

Vue3 + Typescript + Vite + element-plus + eslint + stylelint + prettier + jest

### 环境依赖

node > 12.0

#### 部署步骤

1. npm install
2. npm start or npm run dev
3. npm run build

